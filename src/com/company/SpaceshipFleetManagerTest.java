package com.company;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {
    SpaceshipFleetManager center;
    ArrayList<Spaceship> testList = new ArrayList<>();
    static int point = 0;
    public SpaceshipFleetManagerTest(SpaceshipFleetManager center) {
        this.center = center;
    }
    public static void main(String[] args)  {

        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());


        boolean test1 = spaceshipFleetManagerTest.getMostPowerfulShip_shipExists_returnShip();
        if(test1){
            System.out.println("All good_getMostPowerfulShip_shipExists_returnShip");
        } else {
            System.out.println("Test 1 failed_getMostPowerfulShip_shipExists_returnShip");
        }
        boolean test2 = spaceshipFleetManagerTest.getMostPowerfulShip_shipExists_returnFirstShip();
        if(test2){
            System.out.println("All good_getMostPowerfulShip_shipExists_returnFirstShip");
        } else {
            System.out.println("Test 2 failed_getMostPowerfulShip_shipExists_returnFirstShip");
        }
        boolean test3 = spaceshipFleetManagerTest.getMostPowerfulShip_shipExists_returnNull();
        if(test3){
            System.out.println("All good_getMostPowerfulShip_shipExists_returnNull");
        } else {
            System.out.println("Test 3 failed_getMostPowerfulShip_shipExists_returnNull");
        }
        if(test1 && test2 && test3){
            point++;
        }

        boolean test4 = spaceshipFleetManagerTest.getShipByName_shipExists_returnTargetShip();
        if (test4){
            System.out.println("All good_getShipByName_shipExists_returnTargetShip");
        } else {
            System.out.println("Test 4 failed_getShipByName_shipExists_returnTargetShip");
        }
        boolean test5 = spaceshipFleetManagerTest.getShipByName_shipExists_returnNull();
        if (test5){
            System.out.println("All good_getShipByName_shipExists_returnNull");
        } else {
            System.out.println("Test 5 failed_getShipByName_shipExists_returnNull");
        }
        if(test4 && test5){
            point++;
        }

        boolean test6 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipExists_returnArrayList();
        if(test6){
            System.out.println("All good_getAllShipsWithEnoughCargoSpace_shipExists_returnArrayList");
        } else {
            System.out.println("Test 6 failed_getAllShipsWithEnoughCargoSpace_shipExists_returnArrayList");
        }
        boolean test7 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipExists_returnNull();
        if(test7){
            System.out.println("All good_getAllShipsWithEnoughCargoSpace_shipExists_returnNull");
        } else {
            System.out.println("Test 7 failed_getAllShipsWithEnoughCargoSpace_shipExists_returnNull");
        }
        if(test6 && test7){
            point++;
        }

        boolean test8 = spaceshipFleetManagerTest.getAllCivilianShips_shipExists_returnArrayList();
        if (test8){
            System.out.println("All good_getAllCivilianShips_shipExists_returnArrayList");
        } else {
            System.out.println("Test 8 failed_getAllCivilianShips_shipExists_returnArrayList");
        }
        boolean test9 = spaceshipFleetManagerTest.getAllCivilianShips_shipExists_returnEmptyArrayList();
        if (test9){
            System.out.println("All good_getAllCivilianShips_shipExists_returnEmptyArrayList");
        } else {
            System.out.println("Test 9 failed_getAllCivilianShips_shipExists_returnEmptyArrayList");
        }
        if(test8 && test9){
            point++;
        }


        System.out.println("Test point: " + point);

    }


    private boolean getMostPowerfulShip_shipExists_returnShip(){
        testList.add(new Spaceship("One", 10, 0, 10));
        testList.add(new Spaceship("Two", 0, 0, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        Spaceship result = center.getMostPowerfulShip(testList);
        if (result != null){
            testList.clear();
            return  true;
        } else {
            testList.clear();
            return false;
        }
    }
    private boolean getMostPowerfulShip_shipExists_returnFirstShip(){
        testList.add(new Spaceship("One", 10, 0, 10));
        testList.add(new Spaceship("Two", 10, 0, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        Spaceship result = center.getMostPowerfulShip(testList);
        String name = "One";
        if ((result != null) && (result.getName().equals(name))){
            testList.clear();
            return  true;
        } else {
            testList.clear();
            return false;
        }
    }
    private boolean getMostPowerfulShip_shipExists_returnNull(){
        testList.add(new Spaceship("One", 0, 0, 10));
        testList.add(new Spaceship("Two", 0, 0, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        Spaceship result = center.getMostPowerfulShip(testList);

        if (result == null){
            testList.clear();
            return  true;
        } else {
            testList.clear();
            return false;
        }
    }


    private boolean getShipByName_shipExists_returnTargetShip() {

        testList.add(new Spaceship("One", 10, 0, 10));
        testList.add(new Spaceship("Two", 20, 0, 20));
        testList.add(new Spaceship("Three", 30, 0, 30));

        String testName = "One";
        Spaceship result = center.getShipByName(testList, testName);

        if(result != null && result.getName().equals(testName)){
            testList.clear();
            return  true;
        }
        testList.clear();

        return false;
    }
    private boolean getShipByName_shipExists_returnNull() {

        testList.add(new Spaceship("One", 10, 0, 10));
        testList.add(new Spaceship("Two", 20, 0, 20));
        testList.add(new Spaceship("Three", 30, 0, 30));

        String testName = "On";
        Spaceship result = center.getShipByName(testList, testName);

        if(result == null){
            testList.clear();
            return  true;
        }
        testList.clear();
        return false;
    }


    private boolean getAllShipsWithEnoughCargoSpace_shipExists_returnArrayList() {
        testList.add(new Spaceship("One", 0, 30, 10));
        testList.add(new Spaceship("Two", 0, 20, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        int cargoSize = 20;
        ArrayList<Spaceship> cargoShip = center.getAllShipsWithEnoughCargoSpace(testList, cargoSize);

        if (cargoShip.size() > 0) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }
    private boolean getAllShipsWithEnoughCargoSpace_shipExists_returnNull() {
        testList.add(new Spaceship("One", 0, 0, 10));
        testList.add(new Spaceship("Two", 0, 0, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        int cargoSize = 20;
        ArrayList<Spaceship> cargoShip = center.getAllShipsWithEnoughCargoSpace(testList, cargoSize);

        if (cargoShip.size() == 0) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }

    private  boolean getAllCivilianShips_shipExists_returnArrayList(){
        testList.add(new Spaceship("One", 0, 30, 10));
        testList.add(new Spaceship("Two", 0, 20, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        ArrayList<Spaceship> cargoShip = center.getAllCivilianShips(testList);

        if (cargoShip.size() > 0) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }
    private  boolean getAllCivilianShips_shipExists_returnEmptyArrayList(){
        testList.add(new Spaceship("One", 1, 30, 10));
        testList.add(new Spaceship("Two", 2, 20, 20));
        testList.add(new Spaceship("Three", 3, 0, 30));

        ArrayList<Spaceship> cargoShip = center.getAllCivilianShips(testList);

        if (cargoShip.size() == 0) {
            testList.clear();
            return true;
        }
        testList.clear();
        return false;
    }


}
