package com.company;

import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager{

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        int maxFirePower = 0;
        Spaceship mostPowerfulShip = null;
        for(Spaceship spaceship: ships){
            if ((spaceship.getFirePower() > maxFirePower) && (mostPowerfulShip == null)) {
                maxFirePower = spaceship.getFirePower();
                mostPowerfulShip = spaceship;
            }
        }
        return mostPowerfulShip;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for(Spaceship spaceship: ships){
            if(spaceship.getName().equals(name)){
                return spaceship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> shipsWithEnoughCargoSpace = new ArrayList<>();
        for(Spaceship spaceship:ships){
            if(spaceship.getCargoSpace()>=cargoSize){
                shipsWithEnoughCargoSpace.add(spaceship);
            }
        }
        return shipsWithEnoughCargoSpace;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> civilianShips = new ArrayList<>();
        for(Spaceship spaceship: ships){
            if(spaceship.getFirePower()<=0){
                civilianShips.add(spaceship);
            }
        }
        return civilianShips;
    }
}

