package com.company;

import org.junit.jupiter.api.*;
import java.util.ArrayList;

public class SpaceshipFleetManagerTestUnit {

    static ArrayList<Spaceship> testList = new ArrayList<>();
    static SpaceshipFleetManager centerUnit = new CommandCenter();
    static boolean flag;

    @AfterEach
    public void clearList(){
        testList.clear();
    }

    @DisplayName("getMostPowerfulShip_shipExists_returnShip")
    @Test
    void getMostPowerfulShip_shipExists_returnShip(){
        testList.add(new Spaceship("One", 10, 0, 10));
        testList.add(new Spaceship("Two", 0, 0, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        Spaceship result = centerUnit.getMostPowerfulShip(testList);

        Assertions.assertNotNull(result);
    }

    @DisplayName("getMostPowerfulShip_shipExists_returnFirstShip")
    @Test
    void getMostPowerfulShip_shipExists_returnFirstShip(){
        testList.add(new Spaceship("One", 10, 0, 10));
        testList.add(new Spaceship("Two", 10, 0, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        Spaceship result = centerUnit.getMostPowerfulShip(testList);
        String name = "One";
        if ((result != null) && (result.getName().equals(name))){
            flag = true;
        } else {
            flag = false;
        }
        Assertions.assertTrue(flag);
    }

    @DisplayName("getMostPowerfulShip_shipExists_returnNull")
    @Test
    void getMostPowerfulShip_shipExists_returnNull(){
        testList.add(new Spaceship("One", 0, 0, 10));
        testList.add(new Spaceship("Two", 0, 0, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        Spaceship result = centerUnit.getMostPowerfulShip(testList);

        Assertions.assertNull(result);
    }


    @DisplayName("getShipByName_shipExists_returnTargetShip")
    @Test
    void getShipByName_shipExists_returnTargetShip() {

        testList.add(new Spaceship("One", 10, 0, 10));
        testList.add(new Spaceship("Two", 20, 0, 20));
        testList.add(new Spaceship("Three", 30, 0, 30));

        String testName = "One";
        Spaceship result = centerUnit.getShipByName(testList, testName);

        if(result != null && result.getName().equals(testName)){
            flag =  true;
        } else {
            flag = false;
        }
        Assertions.assertTrue(flag);
    }

    @DisplayName("getShipByName_shipExists_returnNull")
    @Test
    void getShipByName_shipExists_returnNull() {

        testList.add(new Spaceship("One", 10, 0, 10));
        testList.add(new Spaceship("Two", 20, 0, 20));
        testList.add(new Spaceship("Three", 30, 0, 30));

        String testName = "On";
        Spaceship result = centerUnit.getShipByName(testList, testName);

        Assertions.assertNull(result);
    }


    @DisplayName("getAllShipsWithEnoughCargoSpace_shipExists_returnArrayList")
    @Test
    void getAllShipsWithEnoughCargoSpace_shipExists_returnArrayList() {
        testList.add(new Spaceship("One", 0, 30, 10));
        testList.add(new Spaceship("Two", 0, 20, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        int cargoSize = 20;
        ArrayList<Spaceship> cargoShip = centerUnit.getAllShipsWithEnoughCargoSpace(testList, cargoSize);

        if (cargoShip.size() > 0) {
            flag = true;
        } else {
            flag = false;
        }
        Assertions.assertTrue(flag);
    }

    @DisplayName("getAllShipsWithEnoughCargoSpace_shipExists_returnNull")
    @Test
    void getAllShipsWithEnoughCargoSpace_shipExists_returnNull() {
        testList.add(new Spaceship("One", 0, 0, 10));
        testList.add(new Spaceship("Two", 0, 0, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        int cargoSize = 20;
        ArrayList<Spaceship> cargoShip = centerUnit.getAllShipsWithEnoughCargoSpace(testList, cargoSize);

        if (cargoShip.size() == 0) {
            flag = true;
        } else {
            flag = false;
        }
        Assertions.assertTrue(flag);
    }


    @DisplayName("getAllCivilianShips_shipExists_returnArrayList")
    @Test
    void getAllCivilianShips_shipExists_returnArrayList(){
        testList.add(new Spaceship("One", 0, 30, 10));
        testList.add(new Spaceship("Two", 0, 20, 20));
        testList.add(new Spaceship("Three", 0, 0, 30));

        ArrayList<Spaceship> cargoShip = centerUnit.getAllCivilianShips(testList);

        if (cargoShip.size() > 0) {
            flag =  true;
        } else {
            flag = false;
        }
        Assertions.assertTrue(flag);
    }

    @DisplayName("getAllCivilianShips_shipExists_returnEmptyArrayList")
    @Test
    void getAllCivilianShips_shipExists_returnEmptyArrayList(){
        testList.add(new Spaceship("One", 1, 30, 10));
        testList.add(new Spaceship("Two", 2, 20, 20));
        testList.add(new Spaceship("Three", 3, 0, 30));

        ArrayList<Spaceship> cargoShip = centerUnit.getAllCivilianShips(testList);

        if (cargoShip.size() == 0) {
            flag = true;
        } else {
            flag = false;
        }
        Assertions.assertTrue(flag);
    }

}